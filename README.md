# Tienda de cursos online

Proyecto de prueba que muestra un listado de cursos y permite la selección y compra de los mismos

## Requerimientos

* Python 3
* PostgreSQL (puede cambiarse)
* otras dependencias: **requirements.txt**

## Configuración

1. Creación de un entorno virtual e instalación de dependencias

    pip install -r requirements.txt

2. Creación de una base de datos

3. Copiar el archivo de configuración: "settings.json.template" a "settings.json" en el mismo directorio y ajustar los parámetros

4. Ejecutar **./crehana/manage.py runserver**

## TODO

* Crear issues en vez de esta lista
* Vincular el usuario que inicia sesión con el pedido actual
* Permitir limpiar o remover items del carrito de compras
* Enviar correos de notificación
* Integración con Pasarela de pagos