from django.db import models
from uuslug import uuslug


class CreateModel(models.Model):
    created = models.DateTimeField('Fecha de creación', auto_now_add=True)

    class Meta:
        abstract = True


class SlugModel(models.Model):
    nombre = models.CharField('Nombre', max_length=120, db_index=True)
    slug = models.SlugField('slug', max_length=180, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        self.slug = uuslug(self.nombre, instance=self)
        super(SlugModel, self).save(*args, **kwargs)
