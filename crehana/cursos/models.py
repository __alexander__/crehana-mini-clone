from django.db import models
from django.shortcuts import reverse
from core.models import CreateModel
from core.models import SlugModel


class Curso(CreateModel, SlugModel):
    precio = models.IntegerField(default=0, help_text='Precio en soles')
    duracion = models.IntegerField(default=0, help_text='Duración en minutos')
    portada = models.ImageField(upload_to='cursos/portadas/')

    def get_absolute_url(self):
        return reverse('cursos:detalle', kwargs={'slug': self.slug})
