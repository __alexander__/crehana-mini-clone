from django.shortcuts import render
from .models import Curso


def cursos(request, slug=None):
    if slug:
        curso = Curso.objects.get(slug=slug)
        return render(request, 'cursos/curso.html', locals())

    cursos = Curso.objects.all()
    return render(request, 'cursos/cursos.html', locals())
