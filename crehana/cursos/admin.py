from django.contrib import admin
from . import models


@admin.register(models.Curso)
class CursoAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'precio', 'created']
    ordering = ['-created']
