from django.urls import path

from . import views

app_name = 'cursos'

urlpatterns = [
    path('', views.cursos, name='lista'),
    path('curso/<str:slug>/', views.cursos, name='detalle'),
]
