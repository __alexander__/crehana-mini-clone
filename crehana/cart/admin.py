from django.contrib import admin
from . import models


class ItemInline(admin.TabularInline):
    model = models.Item
    extra = 0


@admin.register(models.Cart)
class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'precio_total')
    list_per_page = 25
    inlines = [ItemInline]
    ordering = ['-created']
