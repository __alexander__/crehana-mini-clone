from django.http import JsonResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from cart.cart import Cart, CART_ID
from cursos.models import Curso
from usuarios.constants import LOGIN_URL


def add_to_cart(request):
    id_curso = request.POST.get('id_curso', '').strip()
    try:
        curso = Curso.objects.get(id=id_curso)
    except:
        # en caso de errores, reiniciamos la sesión
        if CART_ID in request.session.keys():
            del request.session[CART_ID]
        return JsonResponse({
            'status': False
        })

    cart = Cart(request)
    cart.add(id_curso, curso.precio)
    response = {'status': True, 'url_redirect': '/cart/'}

    return JsonResponse(response)


@login_required(login_url=LOGIN_URL)
def get_cart(request):
    cart = Cart(request)
    dict_cart = dict(cart)

    return render(request, 'cart/carrito.html', locals())
