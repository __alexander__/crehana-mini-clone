from django.utils import timezone
from . import models
from .constants import CART_ID


class ItemDoesNotExist(Exception):
    pass


class Cart:
    def __init__(self, request):
        cart_id = request.session.get(CART_ID)
        if cart_id:
            try:
                cart = models.Cart.objects.get(id=cart_id)
            except models.Cart.DoesNotExist:
                cart = self.new(request)
        else:
            cart = self.new(request)
        self.cart = cart

    def __iter__(self):
        yield ('total', str(self.total()))
        yield ('cantidad', self.count())

        lista_items = []
        for item in self.cart.item_set.all():
            lista_items.append({
                'id_curso': item.id,
                'nombre': item.curso.nombre,
                'precio': str(item.precio),
                'img': item.curso.portada.url,
                'url': item.curso.get_absolute_url(),
            })
        yield ('items', lista_items)

    def new(self, request):
        cart = models.Cart()
        cart.save()
        request.session[CART_ID] = cart.id
        return cart

    def add(self, id_curso, precio):
        print('id: {} precio: {} '.format(id_curso, precio))
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id_curso=id_curso,
            )
        except models.Item.DoesNotExist:
            item = models.Item()
            item.cart = self.cart
            item.id_curso = id_curso
            item.precio = precio
            item.save()
        else:  # ItemAlreadyExists
            item.unit_price = precio
            item.save()

    def remove(self, id_curso):
        try:
            item = models.Item.objects.get(
                cart=self.cart,
                id_curso=id_curso,
            )
        except models.Item.DoesNotExist:
            raise ItemDoesNotExist
        else:  # ItemAlreadyExists
            item.delete()

    def count(self):
        return self.cart.item_set.count()

    def total(self):
        result = 0
        for item in self.cart.item_set.all():
            result += item.precio
        return result

    def clear(self):
        for item in self.cart.item_set.all():
            item.delete()

    def get_items(self):
        lista_items = []
        for item in self.cart.item_set.all():
            lista_items.append(item)
        return lista_items
