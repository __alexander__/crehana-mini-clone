from django.db import models
from cursos.models import Curso
from . import constants
from core.models import CreateModel


class Cart(CreateModel):
    estado = models.CharField('Estado del Pedido', max_length=2, choices=constants.CARRITO_ESTADO,
                              default=constants.ESTADO_PENDIENTE)

    class Meta:
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'

    def __str__(self):
        return str(self.id)

    def precio_total(self):
        total = 0
        for item in self.item_set.all():
            total = total + item.precio
        return total


class Item(models.Model):
    cart = models.ForeignKey(Cart, verbose_name='Pedido', on_delete=models.PROTECT)
    precio = models.DecimalField(max_digits=18, decimal_places=2, verbose_name='Precio')
    id_curso = models.IntegerField('ID')

    def __str__(self):
        return '{}'.format(self.curso.__class__.__name__)

    @property
    def curso(self):
        return Curso.objects.get(id=self.id_curso)
