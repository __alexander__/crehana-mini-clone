from django.urls import path
from . import views

app_name = 'cart'
urlpatterns = [
    path('', views.get_cart, name='cart'),
    path('add/', views.add_to_cart, name='add_to_cart'),
]
