from django.urls import path
from . import views

app_name = 'cart'
urlpatterns = [
    path('registro', views.signup_view, name='registro'),
    path('ingresar/', views.login_view, name='login'),
    path('salir/', views.logout, name='salir'),
]
