from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Contraseña', min_length=8, max_length=32, widget=forms.PasswordInput)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(username=email)
        except User.DoesNotExist:
            mensaje = 'El email no ha sido registrado'
            raise forms.ValidationError(mensaje)

        return email

    def auth(self):
        cd = self.cleaned_data
        print('::'+cd['email']+'::')
        print('::'+cd['password']+'::')
        return authenticate(username=cd['email'], password=cd['password'])


class SignupForm(forms.Form):
    email = forms.EmailField(label='Email')
    password = forms.CharField(label='Contraseña', min_length=8, max_length=32, widget=forms.PasswordInput)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            User.objects.get(username=email)
            mensaje = 'El email ya existe en nuestra base de datos'
            raise forms.ValidationError(mensaje)
        except User.DoesNotExist:
            return email

    def save(self):
        cd = self.cleaned_data
        user = User.objects.create_user(username=cd['email'], email=cd['email'], password=cd['password'],
                                        is_active=False)
        user.save()
        return user

    def auth(self):
        cd = self.cleaned_data
        return authenticate(username=cd['email'], password=cd['password'])
