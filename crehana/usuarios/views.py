from django.utils import timezone
from django.shortcuts import render, redirect, reverse
from django.urls import reverse_lazy
from django.contrib.auth import login
from django.contrib.auth.views import logout_then_login
from django.contrib import messages

from .forms import SignupForm, LoginForm
from .constants import DJANGO_MODEL_BACKEND, LOGIN_URL


def logout(request):
    return logout_then_login(request, login_url=LOGIN_URL)


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()

            return redirect(reverse_lazy('cart:cart'))
        else:
            errors = form.errors
    else:
        form = SignupForm()
    return render(request, 'users/registro.html', locals())


def login_view(request):
    if request.user.is_authenticated:
        return redirect('cursos:listado')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = form.auth()
            if user:
                if user.is_active:
                    user.last_login = timezone.now()
                    user.save()
                    login(request, user, DJANGO_MODEL_BACKEND)

                    location = request.GET.get('next', None)

                    if location:
                        return redirect(location)

                    return redirect(reverse('cursos:lista'))
                else:
                    messages.add_message(request, messages.WARNING, 'El usuario no se encuentra activo')
            else:
                messages.add_message(request, messages.WARNING, u'El email y contraseña son inválidos')
        else:
            print('Error de formulario')
    else:
        form = LoginForm()

    return render(request, 'users/login.html', locals())
